class CreateTestes < ActiveRecord::Migration
  def change
    create_table :testes do |t|
      t.descicao :string

      t.timestamps
    end
  end
end
