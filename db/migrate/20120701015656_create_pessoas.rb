class CreatePessoas < ActiveRecord::Migration
  def change
    create_table :pessoas do |t|
      t.string :nome
      t.string :telefone
      t.string :celular
      t.string :email
      t.string :endereco
      t.string :numero_imovel
      t.string :bairro
      t.string :cidade
      t.string :estado
      t.string :pais
      t.date :data
      t.string :cpf_cnpj
      t.timestamps
    end
  end
end
